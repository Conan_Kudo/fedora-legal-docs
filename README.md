# Fedora legal documentation

## License

Documentation maintained in this repository is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License (see LICENSES/CC-BY-SA-4.0.txt). Certain other material may be under other terms.

